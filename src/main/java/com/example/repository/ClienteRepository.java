package com.example.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{

}
